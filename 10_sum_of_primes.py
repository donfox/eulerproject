#!/usr/local/bin/python3.6
# 7#_goldbach.py


__author__ = 'donfox1'
"""
Euler_10
"""
#
# the get_primes functions
#
def get_primes1(input_list):
    result_list = list()
    for element in input_list:
        if is_primeP(element):
            result_list.append(element)

    return result_list


def get_primes2(input_list):
    return (element for element in input_list if is_primeP(element))


def get_primes(number):
    while True:
        if is_primeP(number):
            yield number
        number += 1


def get_primes4(number):
    while True:
        if is_primeP(number):
            number = yield number
        number += 1


def is_primeP(number):
    from math import sqrt
    if number > 1:
        if number == 2:
            return True
        if number % 2 == 0:
            return False
        for current in range(3, int(sqrt(number) + 1), 2):
            if number % current == 0:
                return False
        return True
    return False


def solve_number_10():
    """
    Find the sum of all the primes below two million.
    Answer: 142,913,828,922
    """
    total = 2
    for next_prime in get_primes(3)
        if next_prime < 2000000:
            total += next_prime
        else:
            print(total)
            return


def print_successive_primes(iterations, base=10):
    prime_generator = get_primes4(base)
    prime_generator.send(None)
    for power in range(iterations):
        print(prime_generator.send(base ** power))

PrimeList = [2, 3,  5, 7, 11, 13, 17, 19, 23, 29]

print(get_primes1(PrimeList))

print('solve_number_10', solve_number_10())

