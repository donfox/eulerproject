#!/usr/local/bin/python3.7
#
#  Find the first member in the triangle number sequence that has over five hunderd divisors/
#
# https://alexwlchan.net/2019/07/finding-divisors-with-python/
#
from itertools import count
import math, time

def get_tri_nums():
    """generates triangle sequence"""
    t = 0
    for i in count():
        t += i
        yield t
    else:
        yield 0


import collections
import itertools
    
def prime_factors(n):
    i = 2
    while i * i <= n:
        if n % i == 0:
            n /= i
            yield i
        else:
            i += 1

    if n > 1:
        yield n


def prod(iterable):
    result = 1
    for i in iterable:
        result *= i
    return result


def get_divisors(n):
    pf = prime_factors(n)
    pf_with_multiplicity = collections.Counter(pf)

    powers = [
        [factor ** i for i in range(count + 1)]
        for factor, count in pf_with_multiplicity.items()
    ]

    prime_f = []

    for prime_power_combo in itertools.product(*powers):
        prime_f.append(int(prod(prime_power_combo)))
        
    return n, len(prime_f)


if __name__ == "__main__":
    ti = time.time()
    
    tri = get_tri_nums()
    for t in tri:
        tri_num, r = get_divisors(t)
        if r >= 500:
            print(tri_num, r)
            break;
    
    print ("Time taken(secs):", time.time() - ti)

