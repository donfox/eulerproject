
# Large Sum
# Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
# Found in 13_Euler.data

sum = 0
data_file = open('13_Euler.data', 'r')
contents = data_file.readlines()

for x in contents:
	x = int(x)
	sum += x

digits = str(sum)
print(digits[0:10])

data_file.close()