#!/usr/local/bin/python3.7
# What is the sum of the digits of the number 2^1000

big_num = pow(2,1000)
sum = 0
for d in str(big_num):
    sum += int(d)
    
print(sum)