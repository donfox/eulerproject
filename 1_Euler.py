#!/usr/local/bin/python3.6
__author__ = 'donfox1'

"""
Euler_1
If we list all the natural numbers below 10 that are multiples of 3 or 5, we
get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all multiples of 3 or 5 below 1000.

Answer: 233168
"""

# The sum of numbers in below 1000 that are modulus divisible by 3 or 5
print(sum([num for num in range(1001) if ((num % 3 == 0) or (num % 5 == 0))]))