#!/usr/local/bin/python3.7

# What is the index of the first term in the Fibonacci sequence to contain 1000 digits? 

FibArray = [0,1] 
  
def fibonacci(n): 
    if n<0: 
        print("Incorrect input") 
    elif n<=len(FibArray): 
        return FibArray[n-1] 
    else: 
        temp_fib = fibonacci(n-1)+fibonacci(n-2) 
        FibArray.append(temp_fib) 
        return temp_fib 

import math 
def findIndex(n) : 
    fibo = 2.078087 * math.log(n) + 1.672276
   
    # returning rounded off value of index 
    return round(fibo) 

print(fibonacci(8))

for n in range(1,10000):
    if len(str(fibonacci(n))) == 1000:
    	indx = findIndex(fibonacci(n))
        print(fibonacci(n), '-> ', len(str(fibonacci(n))), int(indx))
        break

# FYI:
# fibo generator
#
# def _fibo():
	# a,b,i = 1,1,1
	# while 1:
	# 	yield a,i
	# 	a,b,i = b,a+b,i+1



