#!/usr/local/bin/3.7
#
# The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
#
# How many circular primes are there below one million?


def gen_primes(n):
    ''' Generates primes between 2 and `n` using Wilson's theorem. '''
    fac = 1
    for i in range(2, n):
        fac *= i - 1
        if (fac + 1) % i == 0:
            yield i


def is_prime(x):
    ''' determines if integer is prime '''
    if x >= 2:
        for y in range(2, x):
            if not ( x % y ):
                return False
    else:
        return False
    return True


def is_circular_prime(s):
    '''  Determines if integer is circular prime '''
    digits = len(s)
    cnt = 0
    # does circular permutation of digits and determines
    # whether they are all primes.
    for r in range(digits):
        n = (s[r:] + s[:r])
        n = int(n)
        if is_prime(n):
            cnt += 1
    if cnt == digits:
        #print (n)
        return True
    


circular_cnt = 0
pth = gen_primes(1000000)
for i in pth:
     if is_circular_prime(str(i)):
         circular_cnt += 1

print('circular cnt: ', circular_cnt)





# def numberofDigits(n): 
#     cnt = 0
#     while n > 0: 
#         cnt += 1
#         n //= 10
#     return cnt 




