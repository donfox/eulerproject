#!/usr/local/bin/python3.6
# 
# A prime has factors of 1 and its self

__author__ = 'donfox1'
"""
Euler_3
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143?

Answer:6857
"""

def largest_prime_factor(n):
    '''
    brute force algorithm
    '''
    i = 2
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
    return n

'''
Following are the steps to find all prime factors.
1) While n is divisible by 2, print 2 and divide n by 2.
2) After step 1, n must be odd. Now start a loop from i = 3 to square root of
   n. While i divides n, print i and divide n by i, increment i by 2 and 
   continue.
3) If n is a prime number and is greater than 2, then n
 will not become 1 by above two steps. So print n if it is greater than 2.
'''
from math import ceil, sqrt

def factor(n):
    '''
    Functional prime factor generator
    '''
    if n <= 1: return []
    prime = next((x for x in range(2, ceil(sqrt(n))+1) if n%x == 0), n)
    return [prime] + factor(n//prime)




if __name__ == '__main__':
    
    print(largest_prime_factor(600851475143))
    print(largest_prime_factor(13195))
    print(largest_prime_factor(14))
    print('\n')
    
    
    print(factor(600851475143))
    print(factor(13195))
    print(factor(14))
    
    
    
    

    


