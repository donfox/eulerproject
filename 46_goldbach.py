#!/usr/local/bin/python3.5
# 7#_goldbach.py

__author__ = 'donfox1'
"""
Euler_46
It was proposed by Christian Goldbach (1752) that every odd composite number can be
written as the SUM OF A PRIME AND TWICE A SQUARE.

It turns out that the conjecture was false.

Every odd composite = prime + 2 * x^x.

Find a counterexample, i.e. What is the smallest odd composite that cannot be
written as the sum of a prime and twice a square.

Answer:
"""


def primes(n):
    """Generator function for primes between 2 and `n` using Wilson's theorem."""
    fac = 1
    for i in range(2, n):
        fac *= i - 1
        if (fac + 1) % i == 0:
            yield i


def odds(n):
    """ Generator for odd numbers, including primes. """
    for i in range(2, n):
        if i % 2 != 0:
            yield (i)


def odd_composits(n):
    """ Generator for odd composites. """
    for n in _odds:
        if n in _primes:
            continue
        else:
            yield (n)


primes_iter = primes(6000)
odds_iter = odds(6000)
composits_iter = odd_composits(6000)


def get_counterexample():
    for n in _composits:  # for odd composites
        for p in _primes:
            for i in range(1, 4):
                if n == p + (2 * i ** 2):
                    print('Comp', n, '=', p, '+', '2X', i, ' Sum: ', n)
                    


def goldbach_other_conjecture():
    f, n, primes = 1, 5, {2, 3}
    while True:
        if all(n % p for p in primes):  # If n is a Prime
            primes.add(n)
        elif not any(n - (2 * i ** 2) in primes for i in range(1, n)):
            return n
        n += 3 - f
        f = -f


if __name__ == '__main__':
    _primes = list(primes_iter)
    _odds = list(odds_iter)
    _composits = list(composits_iter)
    get_counterexample()
    #    print(goldbach_other_conjecture())
