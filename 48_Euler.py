#!/usr/local/bin/python3.5
# 48_self_powers.py

__author__ = 'donfox1'

"""
 Euler_48
 The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

 Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.

 Answer: 9110846700
"""

# initial
def sum_of_powers():
    sum = 0
    for i in range(1, 1001):
        sum += i ** i

    digits = str(int(sum))
    return digits[-10:]

# better way
print str(sum([i**i for i in range(1, 1001)]))[-10:]


if __name__=="__main__":
    print(sum_of_powers())

