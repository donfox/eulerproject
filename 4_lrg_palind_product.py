#!/usr/local/bin/python3.5
# 4#_even_fibo.py

__author__ = 'donfox1'
"""
Euler_4
A palindromic number reads the same both ways. The largest palindrome made
from the product of two 2-digit numbers is 9009 = 91 x 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Ansewer: 906609
"""


def generate_lgst_palindrome():
    a = 999
    b = 999
    maximum = 0
    while (a > 100):
        num = a * b
        if is_palindromeP(str(num)):  # collect large palindromes
            if num > maximum:
                maximum = num
        if b >= 100:
            b -= 1
        else:
            a -= 1
            b = 999
    return maximum


def is_palindromeP(word):
    from collections import deque

    dq = deque(word)
    while len(dq) > 1:
        if dq.popleft() != dq.pop():
            return False
    return True

print(generate_lgst_palindrome())
