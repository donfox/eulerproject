#!/usr/local/bin/python3.7.5
# 5#_lcm.py - Lowest Common Multiples

__author__ = 'donfox1'
"""
Euler_5
2520 is the smallest number that can be divided by each of the numbers from
1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20? i.e.

Find smallest positive number which is a multiple of numbers 1..20

Hint: You can compute the LCM of more than two numbers by iteratively
      computing the LCM of two numbers, i.e. lcm(a,b,c) = lcm(a,lcm(b,c))

Answer:
"""


def lcm(x, y):
    """
    This function takes two integers and returns the L.C.M
    """
    if x > y:
        greater = x
    else:
        greater = y

    while (True):
        if (greater % x == 0) and (greater % y == 0):
            low_com_mul = greater
            break
        greater += 1

    return low_com_mul


if __name__ == '__main__':
   print (lcm(2,
         (lcm(3,
         (lcm(4,
         (lcm(5,
         (lcm(6,
         (lcm(7,
         (lcm(8,
         (lcm(9,
         (lcm(10,
         (lcm(11,
         (lcm(12,
         (lcm(13,
         (lcm(14,
         (lcm(15,
         (lcm(16,
         (lcm(17,
         (lcm(18,
         (lcm(19, 20))))))))))))))))))))))))))))))))))))
