#!/usr/local/bin/python3.6
# 6#_sum_sqr_diff.py
import math

__author__ = 'donfox1'

"""
  Euler_6
  Find the difference between the sum of the squares of the first hundred
  natural numbers and the square of the sum of the first hundred natural
  numbers.
  Answer: 25164150
"""


# initial
def sum_of_squares():
    """
    create two lists
    """
    sum_lst = []
    squares_lst = []

    # Load lists for first 100 numbers and their squares respectively, then
    # take sums of each list and subtract latter from the former.
    for n in range(1, 6):
        squares_lst.append(math.pow(n, 2))
        sum_lst.append(n)

    sum_of_sqrs = sum(squares_lst)
    sqr_of_sum = math.pow(sum(sum_lst), 2)

    return sqr_of_sum - sum_of_sqrs


print(sum_of_squares())

# better
print(sum(range(1, 101)) ** 2 - sum(map(lambda x: x ** 2, range(1, 5))))
