#!/usr/local/bin/python3.5
# 7#_1001stPrime.py

__author__ = 'donfox1'
"""
Euler_7
What is the 10001st prime number?

A prime number is a natural number greater than 1 that has no positive divisors
other than 1 and itself.

Answer: 104743
"""


def gen_primes(n):
    """
    Constructs a list of n prime numbers
    """
    from  math import sqrt
    next = 2
    y = [i for i in range(2, n + 1)]
    while y.index(next) + 1 != len(y) and next < sqrt(n):
        y = filter(lambda x: x % next != 0 or x == next, y)
        next = y[y.index(next) + 1]

    print(y[10001 - 1])


if __name__ == '__main__':
    gen_primes(900000)
