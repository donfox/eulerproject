#!/usr/local/bin/python3.5
# coding=utf-8
# 8#_lrgst_product_of_series.py
"""
Euler_8
The four adjacent digits in the 1000-digit number that have the greatest product
are 9 × 9 × 8 × 9 = 5832.

Find the thirteen adjacent digits in the 1000-digit number that have the
greatest product. What is the value of this product?

Answer:
"""

__author__ = 'donfox1'

series = '73167176531330624919225119674426574742355349194934' + \
         '96983520312774506326239578318016984801869478851843' + \
         '85861560789112949495459501737958331952853208805511' + \
         '12540698747158523863050715693290963295227443043557' + \
         '66896648950445244523161731856403098711121722383113' + \
         '62229893423380308135336276614282806444486645238749' + \
         '30358907296290491560440772390713810515859307960866' + \
         '70172427121883998797908792274921901699720888093776' + \
         '65727333001053367881220235421809751254540594752243' + \
         '52584907711670556013604839586446706324415722155397' + \
         '53697817977846174064955149290862569321978468622482' + \
         '83972241375657056057490261407972968652414535100474' + \
         '82166370484403199890008895243450658541227588666881' + \
         '16427171479924442928230863465674813919123162824586' + \
         '17866458359124566529476545682848912883142607690042' + \
         '24219022671055626321111109370544217506941658960408' + \
         '07198403850962455444362981230987879927244284909188' + \
         '84580156166097919133875499200524063689912560717606' + \
         '05886116467109405077541002256983155200055935729725' + \
         '71636269561882670428252483600823257530420752963450'


def get_chunks(s, chunk_size):
    iter = 0;
    while iter < len(s):
        chunk = list(s[iter:chunk_size + iter])
        yield (chunk)
        iter += 1


if __name__ == '__main__':
    chunk_generator4 = get_chunks(series, 13)

    from functools import reduce
    import operator

    max_prod = 0
    curr_chunk = []
    digs = ''
    for chunk in chunk_generator4:
        chunk = [int(i) for i in chunk]
        prod = reduce(operator.mul, chunk)
        if prod > max_prod:
            max_prod = prod
            curr_chunk = chunk

    digs = ' '.join(str(e) for e in curr_chunk)
    print("M_Product: ", max_prod, "for Digs ", digs)
