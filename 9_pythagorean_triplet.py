#!/usr/local/bin/python3.6
# coding=utf-8
# 9#_pythagorian_triplet.py

__author__ = 'donfox1'
"""
Euler_9
There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc
"""

import time

def gcd(a, b):
    while b != 0:
        c = a % b
        a = b
        b = c
    return a


def prod_triplet(n):
    """
    Generator function for some of first n Pythagorean Triplets
    """
    if n > 0:
        for a in range(1, n - 1):
            for b in range(a + 1, n):
                if gcd(a, b) == 1:
                    for c in range(b + 1, n + 1):
                        if (a * a) + (b * b) == (c * c):
                            yield (a, b, c)


# def get_pyth_trips():
#     for i in range(3, 45):
#         a = i
#         b = int(a*a)/2
#         c = b + 1
#         print(a, b, c)

def prod_triplet_w_sum(n):
    for i in range(1,n,1):
        for j in range(1,n-i,1):
            k = n-i-j
            if i**2+j**2==k**2:
                return i*j*k
    return 0


start = time.time()

product = prod_triplet_w_sum(1000)
# pythag_triplet_gen = prod_triplet(1000)

#
# for triple in pythag_triplet_gen:
#     a, b, c = triple
#     print(triple, a + b + c)
#     if a + b + c == 1000:
#         print(a, b, c)

elapsed = (time.time() - start)

print ("found %s in %s seconds" % (product, elapsed))
