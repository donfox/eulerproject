defmodule Euler.Problem9 do
  @moduledoc """
    http://projecteuler.net/problem=9
	A Pythagorean triplet is a set of three natural numbers, a  b  c, for which,
	a^2 + b^2 = c^2
	There exists exactly one Pythagorean triplet for which a + b + c = 1000.
	Find the product abc.
	"""
	
	def solution(n) when n > 0 do
	  for a <- 1..n-2,
	      b <- a+1..n-1,
	      c <- b+1..n,
	      a + b >= c,
	      a*a + b*b == c*c,
				a + b + c == 1000,
	      do: a * b * c
	  end
	end

my_list =  Euler.Problem9.solution(1000)
IO.inspect my_list